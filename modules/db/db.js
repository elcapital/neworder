var mysql = require('mysql');
const { v4: uuidv4 } = require('uuid');

class DB {
  constructor() {
    this.connection = mysql.createConnection({
      host     : '35.201.2.111',
      user     : 'koko',
      password : 'root',
      database : 'highstatus'
    });

    this.connection.connect(function(err) {
      if (err) {
        console.error('error connecting: ' + err.stack);
      }
    });
  }

  QueryBuilder (type, params) { 
    let sql = "INSERT INTO raw_sales";
    params.id = uuidv4();
    this.SanitizeFromPost(params)
    this.CreateTimestamps(params);
    let cols = Object.keys(params);
    sql = this.BuffInsertParams(sql, cols);
    sql = this.BuffValues(sql, cols, params);
    return sql
  }

  CreateTimestamps (params) {
    params.created_at = new Date().toISOString().slice(0, 19).replace('T', ' ');
    params.updated_at = new Date().toISOString().slice(0, 19).replace('T', ' ');
  }

  SanitizeFromPost(params) {
    params.name = `${params.firstname} ${params.lastname}`;
    delete params.firstname;
    delete params.lastname;
    return params;
  }

  BuffInsertParams (sql, cols) {
    sql += "(";
    sql += cols.join(", ");
    sql += ")";
    return sql;
  }

  BuffValues (sql, cols, params) {
    sql += " VALUES (";
    let newArr = cols.map(h => `"${params[h]}"`);
    sql += newArr.join(", ");
    sql += ")";
    return sql;
  }

  async newOrder (data) {
    let sql = this.QueryBuilder('insert', {...data});
    var res = await this.doQuery(sql);
    if(!res) throw new Error ("failed sql");
    return res;
  }

  async test () {
    var res = await this.doQuery("SELECT * FROM raw_sales");
    return JSON.parse(JSON.stringify(res[0], null, 2));

  }

  async doQuery(queryToDo) {
    let pro = new Promise((resolve,reject) => {
        var query = queryToDo;
        this.connection.query(query, function (err, result) {
            if (err) throw err; 
            resolve(result);
        });
    })
    return pro.then((val) => {
        return val;
    })
  }




}

module.exports = DB;