var DB = require('./modules/db/db');

/**
 * Responds to any HTTP request.
 *
 * @param {!express:Request} req HTTP request context.
 * @param {!express:Response} res HTTP response context.
 */
exports.newOrder = async function (req, res)  {
    let message = req.query.message || req.body.message || 'Hello World!';
    try {
        var db = new DB;
        var result = await db.newOrder(req.body);
        res.send(result);
      } catch(e) {
          res.status(400).send(e.message)
      }
};
  